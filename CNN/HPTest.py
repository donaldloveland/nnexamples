from keras import regularizers
from keras import models, layers
from matplotlib import pyplot as plt
from keras.datasets import mnist
from keras.utils import to_categorical
from sys import exit
from sys import stdin


class LyrParams:
    def __init__(self, channels, dropout_rate, regularization):
        self.channels = channels
        self.dropout_rate = dropout_rate
        self.regularization = regularization

    @staticmethod
    def print_param_lyr(param_set):
        print("{} channels {} dropout reg {}".format(param_set.channels,
                                                     param_set.dropout_rate,
                                                     param_set.regularization))

    @staticmethod
    def write_param_lyr(param_set, out):
        out.write("{} chan {} drop {} reg".format(param_set.channels,
                                                  param_set.dropout_rate,
                                                  param_set.regularization))


def model_tune(param_set, out, train_data,
               test_data, train_labels, test_labels):
    for param in param_set:
        LyrParams.write_param_lyr(param, write_file)

    reg_one = 0.1 if param_set[0].regularization is True else 0.0
    reg_two = 0.1 if param_set[1].regularization is True else 0.0
    reg_thr = 0.1 if param_set[2].regularization is True else 0.0
    reg_fou = 0.1 if param_set[3].regularization is True else 0.0

    nn = models.Sequential()
    nn.add(layers.Conv2D(int(param_set[0].channels), (3, 3), activation='relu',
                         input_shape=(28, 28, 1),
                         kernel_regularizer=regularizers.l2(reg_one)))
    nn.add(layers.Dropout(float(param_set[0].dropout_rate)))
    nn.add(layers.Conv2D(int(param_set[1].channels), (3, 3), activation='relu',
                         kernel_regularizer=regularizers.l2(reg_two)))
    nn.add(layers.Dropout(float(param_set[1].dropout_rate)))
    nn.add(layers.MaxPooling2D((2, 2)))
    nn.add(layers.Conv2D(int(param_set[2].channels), (3, 3), activation='relu',
                         kernel_regularizer=regularizers.l2(reg_thr)))
    nn.add(layers.Dropout(float(param_set[2].dropout_rate)))
    nn.add(layers.Conv2D(int(param_set[3].channels), (3, 3), activation='relu',
                         kernel_regularizer=regularizers.l2(reg_fou)))
    nn.add(layers.Dropout(float(param_set[3].dropout_rate)))
    nn.add(layers.MaxPooling2D((2, 2)))
    nn.add(layers.Flatten())
    nn.add(layers.Dense(512, activation='relu'))
    nn.add(layers.Dropout(0.2))
    nn.add(layers.Dense(10, activation='softmax'))
    nn.compile(optimizer='rmsprop', loss='mse', metrics=['accuracy'])

    hst = nn.fit(train_data, train_labels,
                 epochs=8, batch_size=64,
                 validation_data=(test_data, test_labels))
    hst = hst.history

    for i in range(len(hst['acc'])):
        acc = hst['acc'][i]
        loss = hst['loss'][i]
        v_acc = hst['val_acc'][i]
        v_los = hst['val_loss'][i]
        out.write("\t{:.4f} / {:.4f}  {:.4f} / {:.4f}\n".format(acc, loss,
                                                                v_acc, v_los))


def process_data():
    (train_data, train_labels), (test_data, test_labels) = mnist.load_data()

    # Reorganize training and test data as sets of flat vectors
    train_data = train_data.reshape((60000, 28, 28, 1))
    test_data = test_data.reshape((10000, 28, 28, 1))

    # Revise pixel data to 0.0 to 1.0, 32-bit float
    train_data = train_data.astype('float32') / 255
    test_data = test_data.astype('float32') / 255

    # Turn 1-value labels to 10-value vectors
    train_labels = to_categorical(train_labels)
    test_labels = to_categorical(test_labels)
    return ((train_data, train_labels), (test_data, test_labels))


def main():
    # Obtain data properly one hot encoded and organized
    (train_data, train_labels), (test_data, test_labels) = process_data()
    out_file = open("HPTest.out", "w")

    # Go through stdin and generate parameter sets
    line_count = 0
    param_set = []
    for line in stdin:
        # If line is blank, ignore.
        if (not line or '#' in line):
            continue
        # Go through and grab parameters to build parameter set
        else:
            line_count += 1
            line.rstrip('\n')
            params = line.split(' ')
            curr_set = LyrParams(params[0], params[1], params[2])
            param_set.append(curr_set)
        # Every four parameter sets, train model
        if ((line_count % 4) == 0):
            line_count = 0
            model_tune(param_set, out_file, train_data,
                       test_data, train_labels, test_labels)
            del param_set[:]
    out_file.close()


if __name__ == "__main__":
    main()
