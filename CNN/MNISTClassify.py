from keras.models import load_model
from keras.datasets import mnist
import numpy as np


def process_data():
    (train_data, train_labels), (test_data, test_labels) = mnist.load_data()

    test_data = test_data.reshape((10000, 28, 28, 1))
    test_data = test_data.astype('float32') / 255

    return test_data


def main():
    model = load_model('MNIST.h5')
    test_data = process_data()

    predictions = model.predict(test_data)
    for predict in predictions:
        classification = np.argmax(predict)
        print("{}".format(classification))


if __name__ == '__main__':
    main()
