from keras import models, layers
from keras import regularizers
from keras.layers import Input
from keras.models import Model
from keras.utils import to_categorical
from keras.preprocessing.image import ImageDataGenerator
from matplotlib import pyplot as plt
from keras.datasets import mnist
import keras


def create_model_a():
    nn = models.Sequential()
    nn.add(layers.Conv2D(16, (3, 3), activation='relu',
                         input_shape=(28, 28, 1)))
    nn.add(layers.Dropout(0.1))
    nn.add(layers.Conv2D(32, (3, 3), activation='relu'))
    nn.add(layers.Dropout(0.1))
    nn.add(layers.MaxPooling2D((2, 2)))
    nn.add(layers.Conv2D(64, (3, 3), activation='relu'))
    nn.add(layers.Dropout(0.1))
    nn.add(layers.Conv2D(64, (3, 3), activation='relu'))
    nn.add(layers.Dropout(0.1))
    nn.add(layers.MaxPooling2D((2, 2)))
    nn.add(layers.Flatten())
    nn.add(layers.Dense(512, activation='relu'))
    nn.add(layers.Dropout(0.2))
    nn.add(layers.Dense(10, activation='softmax'))
    nn.compile(optimizer='rmsprop', loss='mse', metrics=['accuracy'])
    return nn


def create_model_b():
    nn = models.Sequential()
    nn.add(layers.Conv2D(32, (3, 3), activation='relu',
                         input_shape=(28, 28, 1)))
    nn.add(layers.Dropout(0.1))
    nn.add(layers.Conv2D(32, (3, 3), activation='relu'))
    nn.add(layers.Dropout(0.1))
    nn.add(layers.MaxPooling2D((2, 2)))
    nn.add(layers.Conv2D(32, (3, 3), activation='relu'))
    nn.add(layers.Dropout(0.1))
    nn.add(layers.Conv2D(32, (3, 3), activation='relu'))
    nn.add(layers.Dropout(0.1))
    nn.add(layers.MaxPooling2D((2, 2)))
    nn.add(layers.Flatten())
    nn.add(layers.Dense(512, activation='relu'))
    nn.add(layers.Dropout(0.2))
    nn.add(layers.Dense(10, activation='softmax'))
    nn.compile(optimizer='rmsprop', loss='mse', metrics=['accuracy'])
    return nn


def create_model_c():
    nn = models.Sequential()
    nn.add(layers.Conv2D(32, (3, 3), activation='relu',
                         input_shape=(28, 28, 1)))
    nn.add(layers.Dropout(0.2))
    nn.add(layers.Conv2D(32, (3, 3), activation='relu'))
    nn.add(layers.Dropout(0.2))
    nn.add(layers.MaxPooling2D((2, 2)))
    nn.add(layers.Conv2D(64, (3, 3), activation='relu'))
    nn.add(layers.Dropout(0.2))
    nn.add(layers.Conv2D(64, (3, 3), activation='relu'))
    nn.add(layers.Dropout(0.2))
    nn.add(layers.MaxPooling2D((2, 2)))
    nn.add(layers.Flatten())
    nn.add(layers.Dense(512, activation='relu'))
    nn.add(layers.Dropout(0.2))
    nn.add(layers.Dense(10, activation='softmax'))
    nn.compile(optimizer='rmsprop', loss='mse', metrics=['accuracy'])
    return nn


def create_model_d():
    nn = models.Sequential()
    nn.add(layers.Conv2D(32, (3, 3), activation='relu',
                         input_shape=(28, 28, 1)))
    nn.add(layers.Dropout(0.2))
    nn.add(layers.MaxPooling2D((2, 2)))
    nn.add(layers.Conv2D(64, (3, 3), activation='relu'))
    nn.add(layers.Dropout(0.2))
    nn.add(layers.MaxPooling2D((2, 2)))
    nn.add(layers.Conv2D(64, (3, 3), activation='relu'))
    nn.add(layers.Dropout(0.2))
    nn.add(layers.MaxPooling2D((2, 2)))
    nn.add(layers.Flatten())
    nn.add(layers.Dense(64, activation='relu'))
    nn.add(layers.Dropout(0.2))
    nn.add(layers.Dense(10, activation='softmax'))
    nn.compile(optimizer='rmsprop', loss='mse', metrics=['accuracy'])
    return nn


def process_data():
    (train_data, train_labels), (test_data, test_labels) = mnist.load_data()

    # Reorganize training and test data as sets of flat vectors
    train_data = train_data.reshape((60000, 28, 28, 1))
    test_data = test_data.reshape((10000, 28, 28, 1))

    # Revise pixel data to 0.0 to 1.0, 32-bit float
    train_data = train_data.astype('float32') / 255
    test_data = test_data.astype('float32') / 255

    # Turn 1-value labels to 10-value vectors
    train_labels = to_categorical(train_labels)
    test_labels = to_categorical(test_labels)

    return ((train_data, train_labels), (test_data, test_labels))


def ensemble_models(models, model_input):
    # collect outputs of models in a list
    out_models = [model(model_input) for model in models]
    # averaging outputs
    out_avg = layers.average(out_models)
    # build model from same input and avg output
    model_ens = Model(inputs=model_input, outputs=out_avg, name='ensemble')
    return model_ens


def main():
    (train_data, train_labels), (test_data, test_labels) = process_data()
    gen = ImageDataGenerator(rotation_range=14,
                             shear_range=.2,
                             width_shift_range=.08,
                             height_shift_range=.08,
                             zoom_range=.1)

    models = []

    model_a = create_model_a()
    model_a.name = "16_32_64_64_01mod"

    model_b = create_model_b()
    model_b.name = "32_32_32_32_01mod"

    model_c = create_model_c()
    model_c.name = "32_32_64_64_02mod"

    model_a.fit_generator(gen.flow(train_data, train_labels, batch_size=64),
                          steps_per_epoch=(len(train_data)*6)/64, epochs=5,
                          validation_data=(test_data, test_labels))

    model_b.fit_generator(gen.flow(train_data, train_labels, batch_size=64),
                          steps_per_epoch=(len(train_data)*6)/64, epochs=5,
                          validation_data=(test_data, test_labels))

    model_c.fit_generator(gen.flow(train_data, train_labels, batch_size=64),
                          steps_per_epoch=(len(train_data)*6)/64, epochs=5,
                          validation_data=(test_data, test_labels))

    models.append(model_a)
    models.append(model_b)
    models.append(model_c)

    model_input = Input(shape=models[0].input_shape[1:])
    model_ens = ensemble_models(models, model_input)

    model_ens.save('MNIST.h5')


if __name__ == '__main__':
    main()
